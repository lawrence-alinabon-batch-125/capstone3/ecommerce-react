import React, {useState, useEffect, useContext} from 'react';
import { Card, Container, Form, Button } from 'react-bootstrap';
import UserContext from './../UserContext';
import { Redirect } from 'react-router';
import './../styles/Styles.css';


export default function Login() {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const { user, setUser } = useContext(UserContext)
    const [isDisabled, setIsDisabled] = useState(true);
   
    useEffect(() => {
        if (email !== '' && password !== '') {
            setIsDisabled(false)
        } else {
            setIsDisabled(true)
        } 
    }, [email, password])

    const login = e => {
        e.preventDefault();

        fetch('https://damp-reaches-56047.herokuapp.com/ecommerce/api/users/login',
        {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(result => result.json())
        .then(result => {

            if (typeof result.access !== "undefined") {
                localStorage.setItem('token', result.access)
                userDetails(result.access)
            }
        })

        const userDetails = (token) => {
            fetch('https://damp-reaches-56047.herokuapp.com/ecommerce/api/users/profile',
            {
                method: 'GET',
                headers: {
                    "Authorization" : `Bearer ${token}`
                }
            })
            .then(result => result.json())
            .then(result => {
                setUser({
                    id: result._id,
                    isAdmin: result.isAdmin
                })
                
            })
        }
        
        setEmail('')
        setPassword('')
    }

    return (

        (user.id !== null)
        ?
            <Redirect to="/" />
        :

            <Container className='vh-100'>
                <Card className='shadow login-card p-5 mx-auto my-5'>
                    <Card.Body>
                        <h1 class='text-center'>LOG IN</h1>
                        <Form className='mt-5' onSubmit={login} >
                            
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control
                                    className="p-3"
                                    type="email"
                                    placeholder="Enter email"
                                    value={ email }
                                    onChange={ (e) => setEmail( e.target.value ) }
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control
                                    type="password"
                                    placeholder="Password"
                                    value={ password }
                                    onChange={ (e) => setPassword( e.target.value ) }    
                                />
                            </Form.Group>
                            <Button type="submit" className='w-100 btn-color-red rounded-0 p-3' disabled = {isDisabled}>
                                Submit
                            </Button>

                        </Form>
                    </Card.Body>
                </Card>
            </Container>
    )
}