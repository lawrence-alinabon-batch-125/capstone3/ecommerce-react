import React, {useContext, useEffect, useState} from "react";
import { Container } from "react-bootstrap";
import UserContext from "./../UserContext";
import UserView from "./../components/UserView";
import AdminView from "./../components/AdminView";

export default function Product() {
    
    let token = localStorage.getItem("token");
    const [products, setProducts] = useState([]);

    const {user} = useContext(UserContext)

    const fetchData = () => {
        fetch('https://damp-reaches-56047.herokuapp.com/ecommerce/api/products/all', {
            method: "GET" ,
            headers: {
                "Authorization" : `Bearer ${token}`
            }
        })
        .then(result => result.json())
        .then(result => {
            // console.log(result)
            setProducts(result)
        })
    }

    useEffect(() => {
        fetchData()
    }, [])
    
    return (
        <Container fluid>
        {(user.isAdmin === true) ?
            <AdminView productData={products} fetchData={fetchData}/>
            :
            <UserView productData={products}/>   } 
        </Container>
    )
}