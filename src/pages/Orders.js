import React, {useContext, useEffect, useState} from "react";
import { Container } from "react-bootstrap";
import UserContext from "./../UserContext";
import CartView from "./../components/CartView"; 

export default function Orders() {
    
    let token = localStorage.getItem("token");
    const [orders, setOrders] = useState([]);
    const {user} = useContext(UserContext)

    const fetchData = () => {
        fetch('https://damp-reaches-56047.herokuapp.com/ecommerce/api/orders/', {
            method: "GET" ,
            headers: {
                "Authorization" : `Bearer ${token}`
            }
        })
        .then(result => result.json())
        .then(result => {
            setOrders(result)
        })
    }

    useEffect(() => {
        fetchData()
    }, [])
    
    return (
        <Container fluid>
            <CartView orderData={orders}/>  
        </Container>
    )
}