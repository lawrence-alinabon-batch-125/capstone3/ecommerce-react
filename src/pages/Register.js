import React , {useState, useEffect, useContext}  from "react";
import { Card, Container, Form, Button } from 'react-bootstrap';
import UserContext from './../UserContext';
import { Redirect, useHistory } from 'react-router';
import Swal from 'sweetalert2';

export default function Register() {

    const [firstName, setfirstName] = useState('');
    const [lastName, setlastName] = useState('');
    const [mobileNo, setmobileNo] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [verifyPassword, setVerifyPassword] = useState('');
    const { user, setUser } = useContext(UserContext)
    const [isDisabled, setIsDisabled] = useState(true);

    useEffect(() => {
        if (email !== '' && password !== '' && verifyPassword !== '' && password === verifyPassword) {
            setIsDisabled(false)
        } else {
            setIsDisabled(true)
        } 
    }, [email, password, verifyPassword])

    let history = useHistory();

    function register(e) {
        e.preventDefault();

        // alert('Registration Successful, You may now log in');
        fetch('https://damp-reaches-56047.herokuapp.com/ecommerce/api/users/checkEmail', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(result => result.json())
        .then(result => {
            // console.log(result)

            if (result === true) {
                Swal.fire({
                    title: 'Duplicate email Found',
                    icon:'error',
                    text: 'Please choose another email',
                    button: 'OK'
                })
            } else {
                fetch('https://damp-reaches-56047.herokuapp.com/ecommerce/api/users/register', {
                    method: 'POST',
                    headers: {
                        'Content-Type' : 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        mobileNo: mobileNo,
                        email: email,
                        password : password
                    })
                }).then(result => result.json())
                .then(result => {
                    console.log(result)

                    Swal.fire({
                        title: 'Success',
                        icon:'success',
                        text: 'Account Successfully Registered',
                        button: 'OK'
                    })
                    history.push('/login')

                })
            }
            
        })

        
        setEmail('')
        setPassword('')
        setVerifyPassword('')
    
    }

    return (
        (user.id !== null)
        ?
            <Redirect to="/" />
        :

            <Container className='vh-100'>
                <Card className='shadow login-card p-5 mx-auto my-5'>
                    <Card.Body>
                        <h1 class='text-center'>Register Account</h1>
                        <Form className='mt-5'onSubmit={ (e) => register(e) }>
                            
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>First Name</Form.Label>
                                <Form.Control
                                    className="p-3"
                                    type="text"
                                    placeholder="Enter your first name"
                                    value={ firstName }
                                    onChange={ (e) => setfirstName( e.target.value ) }
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Last Name</Form.Label>
                                <Form.Control
                                    className="p-3"
                                    type="text"
                                    placeholder="Enter your Last Name"
                                    value={ lastName }
                                    onChange={ (e) => setlastName( e.target.value ) }
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Mobile Number</Form.Label>
                                <Form.Control
                                    className="p-3"
                                    type="number  "
                                    placeholder="Enter your mobile number"
                                    value={ mobileNo }
                                    onChange={ (e) => setmobileNo( e.target.value ) }
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control
                                    className="p-3"
                                    type="email"
                                    placeholder="Enter your email address"
                                    value={ email }
                                    onChange={ (e) => setEmail( e.target.value ) }
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control
                                    type="password"
                                    placeholder="Enter your Password"
                                    value={ password }
                                    onChange={ (e) => setPassword( e.target.value ) }    
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Confirm Password</Form.Label>
                                <Form.Control
                                    type="password"
                                    placeholder="Confirm your Password"
                                    value={ verifyPassword }
                                    onChange={ (e) => setVerifyPassword( e.target.value ) }    
                                />
                            </Form.Group>
                            <Button type="submit" className='w-100 btn-color-red rounded-0 p-3' disabled={ isDisabled }> 
                                Submit
                            </Button>
                        </Form>
                    </Card.Body>
                </Card>
            </Container>
    )
}