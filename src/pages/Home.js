import React from 'react';

// bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

import Banner from './../components/Banner';
import Highlights from './../components/Highlights';
import { Container, Card } from 'react-bootstrap';

export default function Home () {
    return (
        <Container fluid>
            <Banner />
            <Card className='p-3'>
                <h4 className='mb-5'>Featured Products</h4>
                <Highlights/>  
            </Card>
                
        </Container>
    )
}