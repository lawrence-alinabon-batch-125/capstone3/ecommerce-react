import React, {useContext, useEffect, useState} from "react";
import { Container } from "react-bootstrap";
import UserContext from "./../UserContext";
import CartView from "./../components/CartView"; 

export default function Cart() {
    
    let token = localStorage.getItem("token");
    const [cart, setCart] = useState([]);
    const {user} = useContext(UserContext)

    const fetchData = () => {
        fetch('https://damp-reaches-56047.herokuapp.com/ecommerce/api/users/profile', {
            method: "GET" ,
            headers: {
                "Authorization" : `Bearer ${token}`
            }
        })
        .then(result => result.json())
        .then(result => {
            setCart(result.cart)
        })
    }

    useEffect(() => {
        fetchData()
    }, [])
    
    return (
        <Container fluid>
            <CartView cartData={cart}/>  
        </Container>
    )
}