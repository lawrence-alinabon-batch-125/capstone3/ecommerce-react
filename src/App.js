import React, {useEffect, useState } from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom';
import UserContext from './UserContext';
import AppNavbar from './components/AppNavbar';
import Login from './pages/Login'
import Register from './pages/Register';
import Product from './pages/Product';
import Cart from './pages/Cart';
import Home from './pages/Home';
import 'bootstrap/dist/css/bootstrap.min.css';
import "bootstrap-icons/font/bootstrap-icons.css";


export default function App() {

    
    const [user, setUser] = useState({
        id: null,
        isAdmin: null
    });

    const unsetUser = () => {
        localStorage.clear();
        setUser({
            id: null,
            isAdmin: null
        })
    }

    useEffect(() => {

        let token = localStorage.getItem('token')
        const getUserDetailsFetch = 'https://damp-reaches-56047.herokuapp.com/ecommerce/api/users/profile';
        const getUserDetailsOptions = {
            method: 'GET',
            headers: {
                "Authorization": `Bearer ${token}`
            }
        };

        fetch(getUserDetailsFetch, getUserDetailsOptions)
        .then(result => result.json())
        .then(result => {
            if (typeof result._id !== 'undefined') {
                setUser({
                    id: result._id,
                    isAdmin: result.isAdmin
                })
            } else {
                 setUser({
                    id: null,
                    isAdmin: null
                })
            }
        })
    }, [])
    return (

        <UserContext.Provider value={{user, setUser, unsetUser}}>
            <BrowserRouter>
            <AppNavbar/>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/cart" component={Cart} />
                    <Route exact path="/products" component={Product} />
                    <Route exact path="/register" component={Register} />
                    <Route exact path="/login" component={Login} />   
                </Switch>
            </BrowserRouter>
        </UserContext.Provider>
        
    )
}