 import React, {useEffect, useState} from 'react';
import {Container, Row , Col, Card, Button} from 'react-bootstrap';
import CartCard from './CartCard';
import Swal from 'sweetalert2'
import {useHistory} from "react-router-dom";

export default function OrderView({orderData}) {

    // console.log( cartData )
    const [orders, setOrders] = useState([]);
    let history = useHistory();
    useEffect(() => {
        const orderArr = orderData.map((orders) => {
                return <CartCard key={cartProd._id} cartProp={cartProd}/>
        })
        
        setCart(cartArr)
    }, [cartData])


    return (
        <Container>
            <Row className="px-3 py-2">
                <Col md={8}>
                    <Card>
                        <div className='my-3 ml-3'>
                            <h5>Cart Items</h5>
                        </div>
                        <Row className="px-3 py-2">
                        {cart}
                        </Row>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}