import React, {useContext} from "react";
import { Col, Row, Card, Button, Image } from "react-bootstrap";
import { Link , useHistory} from "react-router-dom";
import Food from './../images/food.jpg';
import UserContext from "../UserContext";
import Swal from 'sweetalert2'

export default function CartCard({cartProp}) {

    const { productName, productPrice, _id } = cartProp
    let token = localStorage.getItem("token");
    let history = useHistory();
    const { user } = useContext(UserContext)
    
    return (
        <Col xs={12} className='my-2'>
            <Card>
                <Card.Body>
                    {/* <Image src={Food} fluid/> */}
                    <Card.Title className='mt-3'>{ productName }</Card.Title>
                        <h6>Price:</h6>
                        <p>PhP {productPrice} </p>
                </Card.Body>
            </Card>
        </Col>
    )
}