import React, {useEffect, useState} from 'react';
import {Container, Row } from 'react-bootstrap';
import ProductCard from './ProductCard';

export default function UserView({productData}) {

    const [products, setProducts] = useState([]);

    useEffect(() => {
        const productArr = productData.map((product) => {
            if (product.isActive === true) {
                return <ProductCard key={product._id} productProp={ product}/>
            } else {
                return null
            }
        })
        
        setProducts(productArr)
    }, [productData])

    return (
        <Container>
            <Row className ="px-3 py-2">
                {products}
            </Row>
        </Container>
    )
}