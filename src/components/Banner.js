import React, {useContext} from 'react';
import { Button, Col, Row, Jumbotron } from 'react-bootstrap';
import { Link } from "react-router-dom";
import UserContext from "./../UserContext";

export default function Banner() {

    const { user, unsetUser } = useContext(UserContext)


    return (
        <Row>
            <Col className="px-0">
                <Jumbotron fluid className="px-3">
                    <h1>Lawrence Sari-Sari Store</h1>
                    <p>Sari Sari Items for Everyone, Everywhere</p>
                    {
                        (user.id === null) ?
                            <Link className='btn btn-primary' to='/login'> Buy Now </Link>
                        :
                            <Link className='btn btn-primary' to='/products'> Buy Now</Link>
                    }
                </Jumbotron>
            </Col>
        </Row>

)
}