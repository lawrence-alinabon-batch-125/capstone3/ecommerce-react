import React, {useEffect, useState} from 'react';
import {Container, Table, Button, Modal, Form, Card } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props) {

    const {productData, fetchData } = props;
    const [productId, setproductId] = useState('');
    const [products, setProducts] = useState([]);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [showEdit, setShowEdit] = useState(false)
    const [showAdd, setShowAdd] = useState(false)
    
    let token = localStorage.getItem('token')

    const openEdit = (productId) => {
		
		fetch(`https://damp-reaches-56047.herokuapp.com/ecommerce/api/products/${productId}/singleProduct`,{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)

			setproductId(result._id);
			setName(result.productName);
			setDescription(result.productDesc);
			setPrice(result.productPrice)
        })
        
        setShowEdit(true)
    }

    const closeEdit = () => {

        setShowEdit(false);
        setName('');
        setDescription('');
        setPrice(0);
    }

    const updateProduct = (e) => {
        e.preventDefault()

        fetch(`https://damp-reaches-56047.herokuapp.com/ecommerce/api/products/${productId}/updateProduct`, {
            method: 'PUT',
            headers: {
                'Content-Type': "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                productName: name,
                productDesc: description,
                productPrice: price
            })
        })
        .then(result => result.json())
        .then(result => {
            
            fetchData()
            if (typeof result !== "undefined") {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Product Successfully Updated'

                })
                
                closeEdit();
            } else {
                fetchData()
                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: 'Product Not Updated'

                })
                closeEdit();
            }
        })
    }

    const openAdd = () => setShowAdd(true);

    const closeAdd = () => setShowAdd(false);

    const addProduct = (e) => {
        e.preventDefault();

        fetch(`https://damp-reaches-56047.herokuapp.com/ecommerce/api/products/addProduct`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                productName: name,
                productDesc: description,
                productPrice: price
            })
        })
        .then(result => result.json())
        .then(result => {
            console.log(result)
            if (result !== undefined) {
                fetchData()
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Product Successfully Added'

                })
                setName('')
                setDescription('')
                setPrice(0)
                closeAdd()
            } else {
                fetchData();
                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: 'Product Not Added'

                })
                closeAdd()
            }
        })
    }

    const archiveToggle = (productId, isActive) => {
        fetch(`https://damp-reaches-56047.herokuapp.com/ecommerce/api/products/${productId}/archiveProduct`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                isActive: isActive
            })
        })
        .then(result => result.json())
        .then(result => {
                fetchData();
            if (result === true) {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Product Successfully Archived'

                })
            } else {
                fetchData();
                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: 'Product Not Archived'

                })
            }
        })
    }

    const unarchiveToggle = (productId, isActive) => {
		fetch(`https://damp-reaches-56047.herokuapp.com/ecommerce/api/products/${productId}/unarchiveProduct`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)

			fetchData();
			if(result === true){
				Swal.fire({
					title: "Success",
					icon: "success",
					"text": "Product successfully unarchived"
				})
			} else {
				fetchData();
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					"text": "Please try again"
				})
			}
		})
	}


	useEffect( () => {
		const productArr = productData.map( (product) => {
			return(
				<tr key={product._id}>
					<td>{product.productName}</td>
					<td>{product.productDesc}</td>
					<td>&#8369; {product.productPrice}</td>
					<td>
						{
							(product.isActive === true) ?
								<span className='bg-success px-3 py-1'>Available</span>
							:
								<span className='bg-danger px-2 py-1 '>Unavailable</span>
						}
					</td>
					<td>
						<Button variant="primary" size="sm" 
						 onClick={ ()=> openEdit(product._id) }>
							Update
                        </Button>
                        
                        {
                            (product.isActive === true) ?
                                <Button variant="danger" size="sm" className='mx-2'
                                onClick={()=> archiveToggle(product._id, product.isActive)}>
                                    Disable
                                </Button>
                            :
                                <Button variant="success" size="sm"
                                className='mx-2' onClick={ () => unarchiveToggle(product._id, product.isActive)}>
                                    Enable
                                </Button>
                        }
                        
					</td>
				</tr>
			)
		})

		setProducts(productArr)
	}, [productData])


    return (
        <Container>
            <Card className='glass-card my-5 py-5 px-3'>
                <div>
                <h2 className="text-center">Admin Dashboard</h2>
                <div className="d-flex justify-content-end mb-2"> 
                    <Button variant='primary' onClick={openAdd}>Add New Product</Button>
                </div>
                </div>
                <Card>
                    <Table>
                        <thead>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Availability</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            {/*display the courses*/}
                            {products}

                        </tbody>
                    </Table>
                
                </Card>
            
            </Card>
            
            <Modal show={showEdit} onHide={closeEdit}>
                <Form onSubmit={ (e) => updateProduct(e, productId)}>
                    <Modal.Header>
                        <Modal.Title>Edit A Course</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group controlId='courseName'>
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type='text'
                                value={name}
                                onChange={ (e) => setName(e.target.value)} />
                        </Form.Group>
                        <Form.Group controlId='courseDescription'>
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                type='text'
                                value={description}
                                onChange={ (e) => setDescription(e.target.value)} />
                        </Form.Group>
                        <Form.Group controlId='coursePrice'>
                            <Form.Label>Price</Form.Label>
                            <Form.Control
                                type='number'
                                value={price}
                                onChange={ (e) => setPrice(e.target.value)} />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeEdit}>Close</Button>
                        <Button variant="success" type='submit'>Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
            

            <Modal show={showAdd} onHide={closeAdd}>
                <Form onSubmit={(e) => addProduct(e)}>
                    <Modal.Header>
                        <Modal.Title>Add Product</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group controlId='courseName'>
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type='text'
                                value={name}
                                onChange={ (e) => setName(e.target.value)} />
                        </Form.Group>
                        <Form.Group controlId='courseDescription'>
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                type='text'
                                value={description}
                                onChange={ (e) => setDescription(e.target.value)} />
                        </Form.Group>
                        <Form.Group controlId='coursePrice'>
                            <Form.Label>Price</Form.Label>
                            <Form.Control
                                type='number'
                                value={price}
                                onChange={ (e) => setPrice(e.target.value)} />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeAdd}>Close</Button>
                        <Button variant="success" type='submit'>Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </Container>
    )
}
