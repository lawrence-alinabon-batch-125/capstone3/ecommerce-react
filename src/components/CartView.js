import React, {useEffect, useState} from 'react';
import {Container, Row , Col, Card, Button} from 'react-bootstrap';
import CartCard from './CartCard';
import Swal from 'sweetalert2'
import {useHistory} from "react-router-dom";

export default function CartView({cartData}) {

    console.log( cartData )
    const [cart, setCart] = useState([]);
    let history = useHistory();

    let total = cartData.reduce((total, price) => {
        return total + price.productPrice
    }, 0)

    let items = cartData.length;

    const checkout = () => {
        let token = localStorage.getItem('token')


        fetch(`https://damp-reaches-56047.herokuapp.com/ecommerce/api/orders/checkout`,
        {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`

            },
            body: JSON.stringify({
                totalAmount: total,
                productsOrdered: cartData
            })
        })
        .then(result => result.json())
        .then(res => {
            console.log(res)

            if (res) {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Order Successfully Created!'

                });
                history.push('/products')
            } else {
                alert("Something went wrong")
            }
        
        })

    }

    useEffect(() => {
        const cartArr = cartData.map((cartProd) => {
                return <CartCard key={cartProd._id} cartProp={cartProd}/>
        })
        
        setCart(cartArr)
    }, [cartData])


    return (
        <Container>
            <Row className="px-3 py-2">
                <Col md={8}>
                    <Card>
                        <div className='my-3 ml-3'>
                            <h5>Cart Items</h5>
                        </div>
                        <Row className="px-3 py-2">
                        {cart}
                        </Row>
                    </Card>
                </Col>
                <Col md={4}>
                    <Card className="px-3 py-2">
                        <p className='mt-2'> {items} items in the cart</p>
                        <p className='mt-2'>Total Amount to Pay: </p>
                        <h4>PhP {total} </h4>
                        <Button variant="primary" className='mt-5' onClick={ () => checkout()}>Proceed to Checkout</Button>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}