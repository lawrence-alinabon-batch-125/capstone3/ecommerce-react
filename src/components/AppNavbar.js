import React, { Fragment, useContext} from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link, NavLink, useHistory } from 'react-router-dom';
import UserContext from './../UserContext';

export default function AppNavbar() {

    const { user, unsetUser } = useContext(UserContext)
    let history = useHistory();

    const logout = () => {
        unsetUser();
        history.push('/login')
    }

    let leftNav = (user.id !== null)
    ? (
        (user.isAdmin === true)
        ?(
            <Fragment >
                <Nav.Link as={NavLink} to="/products" className='mr-5'><i class="bi bi-bag-fill mr-1"></i> Products </Nav.Link>
                <Nav.Link as={NavLink} to="/cart" className='mr-5'><i class="bi bi-cart-fill mr-1"></i> Orders </Nav.Link>
                <Nav.Link onClick={logout}><i class="bi bi-box-arrow-left"></i> Logout </Nav.Link>
            </Fragment>
        ) : (
            
            <Fragment >
                <Nav.Link as={NavLink} to="/products" className='mr-5'><i class="bi bi-bag-fill mr-1"></i> Products </Nav.Link>
                <Nav.Link as={NavLink} to="/cart" className='mr-5'><i class="bi bi-cart-fill mr-1"></i> Cart </Nav.Link>
                <Nav.Link onClick={logout}><i class="bi bi-box-arrow-left"></i> Logout </Nav.Link>
            </Fragment>
        )
    ) : (
        <Fragment >
            <Nav.Link as={NavLink} to="/register" className='mr-5'><i class="bi bi-person-lines-fill mr-1"></i> Register </Nav.Link>
            <Nav.Link as={NavLink} to="/login" ><i class="bi bi-box-arrow-in-right mr-1"/> Login </Nav.Link>
        </Fragment>
    )
        
    return (
        <Navbar className='navbar navbar-expand-lg navbar-light bg-light shadow py-4' expand="lg">
        <Navbar.Brand as={Link} to="/">
            Lawrence Sari<sup>2x</sup> Store ver. 2.0
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className='ml-auto'>
                <Nav.Link as={NavLink} to="/" className='mr-5'><i class="bi bi-house-door-fill mr-1"></i> Home </Nav.Link>
                {leftNav}
            </Nav>
        </Navbar.Collapse>
    </Navbar>
    )
}