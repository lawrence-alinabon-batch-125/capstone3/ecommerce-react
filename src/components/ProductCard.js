import React, {useContext} from "react";
import { Col, Row, Card, Button, Image } from "react-bootstrap";
import { Link , useHistory} from "react-router-dom";
import Food from './../images/food.jpg';
import UserContext from "./../UserContext";
import Swal from 'sweetalert2'

export default function ProductCard({productProp}) {

    const { productName, productDesc, productPrice, _id } = productProp
    let token = localStorage.getItem("token");
    let history = useHistory();
    const { user } = useContext(UserContext)
    
    const addtocart = () => {
        fetch(`https://damp-reaches-56047.herokuapp.com/ecommerce/api/users/addtocart`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                productId: _id,
                productPrice: productPrice,
                productName : productName
            })
        })
        .then(result => result.json())
        .then(result => {
        if (result === true) {
            Swal.fire({
                title: 'Success',
                icon: 'success',
                text: 'Item Successfully Added'

            });
            history.push('/products')
        } else {
            Swal.fire({
                title: 'Error',
                icon: 'error',
                text: 'Something went wrong'

            })
                
            }
        })
    }

    return (
        <Col xs={4} className='my-2'>
            <Card>
                <Card.Body>
                    <Image src={Food} fluid/>
                    <Card.Title className='mt-3'>{ productName }</Card.Title>
                        <h6>Description:</h6>
                        <p> {productDesc} </p>
                        <h6>Price:</h6>
                        <p><b>PhP {productPrice} </b></p>

                        {
                        (user.id !== null) ?
                            <Button variant="primary" onClick={ () => addtocart()}>Add to Cart</Button>
                        :
                            <Link className='btn btn-primary' to='/login'> Log in to enroll </Link>    
                        }
                </Card.Body>
            </Card>
        </Col>
    )
}