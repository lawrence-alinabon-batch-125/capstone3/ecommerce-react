import React from "react";
import { Col, Row, Card, Button, Image } from "react-bootstrap";
import Food from './../images/food.jpg';
export default function Highlights() {

    

    return (
        <Row>
            <Col xs={12} md={4}>
                <Card>
                   <Card.Body>
                    <Image src={Food} fluid/>
                    <Card.Title className='mt-3'>Natures Spring Water</Card.Title>
                        <h6>Price:</h6>
                        <p><b>PhP 20</b></p>

        
                </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card>
                    <Card.Body>
                    <Image src={Food} fluid/>
                    <Card.Title className='mt-3'>Ace Biscuit</Card.Title>
                        <h6>Price:</h6>
                        <p><b>PhP 50</b></p>


                </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card>
                    <Card.Body>
                    <Image src={Food} fluid/>
                    <Card.Title className='mt-3'>Nissin Spicy Bulalo</Card.Title>
                        <h6>Price:</h6>
                        <p><b>PhP 25</b></p>
                </Card.Body>
                </Card>
            </Col>
        </Row>
        
    )
}